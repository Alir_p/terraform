data "vkcs_compute_flavor" "compute" {
  name = "Basic-1-1-10"
}

data "vkcs_images_image" "compute" {
  name = "Ubuntu-22.04-202208"
}


variable "key_pair_name" {
  type = string
  default = "Alir_VOYADGER-rsa"
}