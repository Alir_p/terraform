

resource "vkcs_sharedfilesystem_sharenetwork" "sharenetwork1" {
  name                = "test_sharenetwork1"
  neutron_net_id      = "${vkcs_networking_network.network.id}"
  neutron_subnet_id   = "${vkcs_networking_subnet.subnetwork.id}"
}

resource "vkcs_sharedfilesystem_share" "share1" {
  name             = "nfs_share1"
  description      = "test share description"
  share_proto      = "NFS"
  share_type       = "default_share_type"
  size             = 1
  share_network_id = "${vkcs_sharedfilesystem_sharenetwork.sharenetwork1.id}"
}

resource "vkcs_sharedfilesystem_share_access" "share_access_1" {
  share_id     = "${vkcs_sharedfilesystem_share.share1.id}"
  access_type  = "ip"
  access_to    = "10.10.161.110"
  access_level = "rw"
}

resource "vkcs_sharedfilesystem_share_access" "share_access_2" {
  share_id     = "${vkcs_sharedfilesystem_share.share1.id}"
  access_type  = "ip"
  access_to    = "10.10.161.111"
  access_level = "rw"
}