# vkcs_networking_network — сеть, в которую будут вноситься изменения.
# vkcs_networking_subnet — подсеть из сети. В примере: subnetwork.
# vkcs_networking_router — роутер для внешней сети и взаимодействия с внешним миром. В примере: router.
# vkcs_networking_router_interface — подключить роутер к внутренней сети.

data "vkcs_networking_network" "extnet" {
  name = "ext-net"
}

resource "vkcs_networking_network" "network" {
   name = "net"
   admin_state_up = "true"
}

resource "vkcs_networking_subnet" "subnetwork" {
   name       = "subnet"
   network_id = vkcs_networking_network.network.id
   cidr       = "10.10.161.0/24"
   allocation_pool {
     end   = "10.10.161.99"
     start = "10.10.161.50"
  }
}

resource "vkcs_networking_router" "router" {
   name                = "router"
   admin_state_up      = true
   external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "network" {
  router_id = vkcs_networking_router.router.id
  subnet_id = vkcs_networking_subnet.subnetwork.id
}

